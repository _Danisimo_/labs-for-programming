// let timeInSeconds = 60;

// document.getElementById("timer").textContent = timeInSeconds;

// setInterval(()=>{
//     timeInSeconds--;
//     document.getElementById("timer").textContent = timeInSeconds;
// }, 1000);

let firstArray = ['Text'];
let secondArray = ['Text', 'Another Text'];
let thirdArray = ['Fruits', 'Vegetables', 'Meat'];

function showArray(array) {

    switch(array.length) {
        case 1: {
            
            alert(array[0]);
            break;
        }
        case 2: {

            alert(array[0] + " and " + array[1]);
            break;
        }
        default: {

            let compose = "";
            for(let index = 0;index<array.length;index++) {
                if(index==0) {
                    compose = array[index];
                }
                else if(index==array.length-1) {
                    compose = compose + " and " + array[index];
                }
                else {
                    compose = compose + ", " + array[index];
                }
            }
            alert(compose);
            break;
        }
    }

}

showArray(firstArray);
showArray(secondArray);
showArray(thirdArray);