import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;

public class Controller {

    @FXML
    BorderPane pane;

    private MultipleBallPane ballPane = new MultipleBallPane();

    public void initialize() {

        ballPane.setOnMouseReleased(e -> ballPane.add(e.getX(),e.getY()));
        ballPane.rateProperty().setValue(10);
        pane.setCenter(ballPane);

    }

    @FXML
    private void stop(){
        ballPane.pause();
    }
    @FXML
    private void run(){
        ballPane.play();
    }
    @FXML
    private void remove(){
        ballPane.subtract();
    }
}
