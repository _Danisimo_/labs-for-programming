import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.sqrt;

class MultipleBallPane extends Pane {

    private Timeline animation;
    private List arrayOfBalls = new ArrayList();


    MultipleBallPane() {
        animation = new Timeline(
                new KeyFrame(Duration.millis(50), e -> moveBall()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();

    }

    void add(double x, double y) {

        new Thread(() -> Platform.runLater(() -> {
            Color color = new Color(Math.random(),
                    Math.random(), Math.random(), 1);
            getChildren().add(new Ball(x, y, Ball.rnd(20,50), color));
        })).start();
        arrayOfBalls.add(null);
    }

    void subtract() {
        if (getChildren().size() > 0) {
            getChildren().remove(getChildren().size() - 1);
        }
        arrayOfBalls.remove(0);
    }

    void play() {
        animation.play();
    }

    void pause() {
        animation.pause();
    }

    DoubleProperty rateProperty() {
        return animation.rateProperty();
    }

    private void moveBall() {
        Ball ball1;
        Ball ball2 =null;
        boolean a =false;
        ObservableList<Node> children = this.getChildren();
        for (int i = 0; i < children.size(); i++) {

            ball1 = (Ball) children.get(i);

            if (ball1.getCenterX() < ball1.getRadius() ||
                    ball1.getCenterX() > getWidth() - ball1.getRadius()) {
                ball1.dx *= -1;
            }
            if (ball1.getCenterY() < ball1.getRadius() ||
                    ball1.getCenterY() > getHeight() - ball1.getRadius()) {
                ball1.dy *= -1;
            }

            ball1.setCenterX(ball1.dx + ball1.getCenterX());
            ball1.setCenterY(ball1.dy + ball1.getCenterY());

            for (int nextBall = i + 1; nextBall < arrayOfBalls.size(); nextBall++ ) {
                try {
                    ball2 = (Ball) children.get(nextBall);
                }catch (IndexOutOfBoundsException ignored){}
                double deltaX = ball2.getCenterX() - ball1.getCenterX();
                double deltaY = ball2.getCenterY() - ball1.getCenterY();
                double radiusSum = ball1.getRadius() + ball2.getRadius();
                if (deltaX * deltaX + deltaY * deltaY <= radiusSum * radiusSum) {
                    if (deltaX * (ball2.dx - ball1.dx)
                            + deltaY * (ball2.dy - ball1.dy) < 0) {
                        bounce(ball1, ball2, deltaX, deltaY);
                    }
                }
            }

        }
    }

    private void bounce(final Ball ball1, final Ball ball2, final double deltaX, final double deltaY) {

        double distance = sqrt(deltaX * deltaX + deltaY * deltaY);
        double unitContactX = deltaX / distance;
        double unitContactY = deltaY / distance;

        double xVelocity1 = ball1.dx;
        double yVelocity1 = ball1.dy;
        double xVelocity2 = ball2.dx;
        double yVelocity2 = ball2.dy;

        double u1 = xVelocity1 * unitContactX + yVelocity1 * unitContactY;
        double u2 = xVelocity2 * unitContactX + yVelocity2 * unitContactY;

        double massSum = ball1.mass + ball2.mass;
        double massDiff = ball1.mass - ball2.mass;

        double v1 = ((2 * ball2.mass) * u2 + u1 * massDiff) / massSum;
        double v2 = ((2 * ball1.mass) * u1 - u2 * massDiff) / massSum;

        double u1PerpX = xVelocity1 - u1 * unitContactX;
        double u1PerpY = yVelocity1 - u1 * unitContactY;
        double u2PerpX = xVelocity2 - u2 * unitContactX;
        double u2PerpY = yVelocity2 - u2 * unitContactY;

        ball1.dx=(v1 *unitContactX + u1PerpX);
        ball1.dy=(v1 *unitContactY + u1PerpY);
        ball2.dx=(v2 *unitContactX + u2PerpX);
        ball2.dy=(v2 *unitContactY + u2PerpY);

    }

}