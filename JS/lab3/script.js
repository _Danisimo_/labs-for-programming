setInterval(() => {
    var d = new Date();
    currentHour = d.getHours();
    currentMinute = d.getMinutes();
    currentSecond = d.getSeconds();
    if(currentHour.toString().length==1) currentHour = "0"+currentHour;
    if(currentMinute.toString().length==1) currentMinute = "0"+currentMinute;
    if(currentSecond.toString().length==1) currentSecond = "0"+currentSecond;
    document.getElementById("clock").textContent = currentHour + ":" + currentMinute + ":" + currentSecond;
}, 1000);

function createCalendar(id, year, month) {
    var elem = document.getElementById(id);

    var mon = month - 1;
    var d = new Date(year, mon);

    var table = '<table><tr><th>mon</th><th>tue</th><th>wed</th><th>thr</th><th>fri</th><th>sat</th><th>sun</th></tr><tr>';


    for (var i = 0; i < getDay(d); i++) {
      table += '<td></td>';
    }

    while (d.getMonth() == mon) {
      table += '<td>' + d.getDate() + '</td>';

      if (getDay(d) % 7 == 6) {
        table += '</tr><tr>';
      }

      d.setDate(d.getDate() + 1);
    }

    // добить таблицу пустыми ячейками, если нужно
    if (getDay(d) != 0) {
      for (var i = getDay(d); i < 7; i++) {
        table += '<td></td>';
      }
    }

    // закрыть таблицу
    table += '</tr></table>';

    // только одно присваивание innerHTML
    elem.innerHTML = table;
  }

  function getDay(date) { // получить номер дня недели, от 0(пн) до 6(вс)
    var day = date.getDay();
    if (day == 0) day = 7;
    return day - 1;
  }



  createCalendar("calendar", 2019, 3)