window.onload = () => {
    let container = document.getElementsByClassName("container")[0];


    //creating paragraphs
    let header = document.createElement("h1");
    header.textContent = "Those are generated paragraphs.";
    container.appendChild(header);



    for(let index = 0;index<4;index++) {
        let paragraph = document.createElement("p");
        switch(index) {
            case 0: {
                paragraph.style.cssText = "font-size: 20px; font-style: italic; color: blue";
                break;
            }
            case 1: {
                paragraph.style.cssText = "font-size: 16px; font-weight: bold; color: brown;"
                break;
            }
            case 2: {
                paragraph.style.cssText = "font-size: 10px; text-decoration: underline; color: red;"
                break;
            }
            default: {
                paragraph.style.fontSize = "12px";
                break;
            }
        } 
        container.appendChild(paragraph);
    }

    //creating marked list
    let listDiv = document.createElement("div");
    let ul = document.createElement("ul");

    for(let index = 0; index<3;index++) {
        let li = document.createElement("li");
        li.textContent = `This is an element number ${index}`;
        ul.appendChild(li);
    }
    listDiv.appendChild(ul);
    container.appendChild(listDiv);

    //creating numerated list
    let numListDiv = document.createElement("div");
    let ol = document.createElement("ol");

    for(let index = 0;index<3;index++)
    {
        let li = document.createElement("li");
        li.textContent = `This is an element number ${index}`;
        ol.appendChild(li);
    }
    numListDiv.appendChild(ol);
    container.appendChild(numListDiv);
};